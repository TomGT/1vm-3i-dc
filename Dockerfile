# Use the base Squash TM image
FROM squashtest/squash-tm:5.1.0

# Set the working directory
WORKDIR /opt/squash-tm

# Add the plugins to the appropriate directories
COPY automation.result.publisher.community-5.0.0.RELEASE.jar /opt/squash-tm/plugins
COPY automation.scm.git-5.0.0.RELEASE.jar /opt/squash-tm/plugins
COPY org.eclipse.jgit-6.4.0.202211300538-r.jar /opt/squash-tm/plugins
COPY automation.squashautom.community-5.0.0.RELEASE.jar /opt/squash-tm/plugins

### BUILD IMAGE
# docker build -t tomtomgt/squash-tm-saas-deploy:1.0 .
# docker push tomtomgt/squash-tm-saas-deploy:1.0


### CHECK IMAGE
# cd 'G:\My Drive\Dev_Cours\DevOps\Docker'
# docker image ls
# docker image inspect tomtomgt/squash-tm-saas-deploy:1.0

### RUN IMAGE
# docker run -it --rm tomtomgt/squash-tm-saas-deploy:1.0 /bin/bash
# docker run --name='squash-tm' -it -p 8090:8080 tomtomgt/squash-tm-saas-deploy:1.0
# docker logs -f squash-tm

### STOP IMAGE
# docker stop squash-tm
# docker rm squash-tm
# docker rmi tomtomgt/squash-tm-saas-deploy:1.0

### Connect to Squash
# http://localhost:8090/squash

### Check plugin installation
# http://localhost:8090/squash/administration-workspace/servers/test-automation-servers
# => Administration => Server => Test Automation Server => add plugins (see if you can access the new plugins)